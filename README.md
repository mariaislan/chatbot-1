# Chatbot guía del centro comercial PlanetaOcio #

Este Chatbot tiene el objetivo de informar a los clientes de las ubicaciones de las diferentes tiendas y servicios que ofrece el centro comercial PlanetaOcio.


### ¿Cómo correr el proyecto? ###

Se debe correr el fichero main: "ChatbotFinal.py" enlazado al fichero "ContenidoA.json".

### Este trabajo fue elaborado por: ###

-Delgado Jiménez, María Del Valle

-Islán Moriñigo, María

-Magro Canas, Andrea

-Romero Gutiérrez, Kendra Gabriela
